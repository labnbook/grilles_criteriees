# Docker-compose-laravel
Ce backend utilise une pile docker dont le dépôt est [ici](https://github.com/aschmelyun/docker-compose-laravel).

Toutes les instructions relatives à son usage sont disponibles sur le dépôt. 

Les éléments qui suivent précisent les aménagements spécifiques au backend des grilles critériées. 

# Installation
1. Se placer à l'endroit où l'on souhaite installer le backend puis cloner ce dépôt :

```bash
cd monDossier
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/labnbook/grilles_criteriees.git
```

2. Créer un dossier `mysql` à la racine du projet :

```bash
cd grilles_criteriees
mkdir mysql
```

3. Copier le ficher `.env.example` puis le renommer en `.env` :

```bash
cd src
cp .env.example .env
```

4. Dans le fichier `.env`, fixer les paramètres suivants : 

```
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=laravel
DB_PASSWORD=secret
```

Il convient également de spécifier le token JWT ou bien désactiver la vérification JWT (en fin de fichier).

5. Démarrer le conteneur docker :

```bash
cd ..
./gc up
```

6. Dans un autre terminal, installer les dépendances puis créer une clé pour l'application Laravel :

```bash 
./gc composer_install
./gc generate_key
```

7. Lancer les migrations :

```bash 
./gc migrate
```

8. Il est également possible d'effacer toutes les données, de relancer toutes les migrations puis de peupler la base avec des données simulées :

```bash
./gc migrate_from_scratch
```

## Sans docker

La procédure ci-dessus fonctionnera mais ne rendra pas le service accessible.

Voici un exemple de virtual host pour qu'il soit accessible uniquement sur un serveur local via le port 88, avec apache :

```
<VirtualHost *:88>
        ServerName localhost

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/labnbook/grilles_criteriees/src/public

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/labnbook_gc_error.log
        CustomLog ${APACHE_LOG_DIR}/labnbook_gc_access.log combined

    # increase security
    <Directory "/var/www/labnbook/grilles_criteriees/src/public/files">
        <FilesMatch "\.ph(p[3457]?|t|tml|ps)$">
            Require all denied
        </FilesMatch>
    </Directory>
    <Directory "/var/www/labnbook/grilles_criteriees/src/public">
        Require all granted
        Options +FollowSymLinks -Indexes
        AllowOverride All
    </Directory>
    <FilesMatch \.php$>
      # For Apache version 2.4.10 and above, use SetHandler to run PHP as a fastCGI process server
      SetHandler "proxy:unix:/run/php/php8.1-fpm.sock|fcgi://localhost"
    </FilesMatch>
    <Location />
        Require ip 127.0.0.1 ::1
    </Location>


</VirtualHost>
```

**ne pas oublier** d'ajouter l'instruction `Listen 88` dans `/etc/apache2/ports.conf`

# Fonctionnement de l'API
## Sans JWT
Modifier le fichier `.env` en bloquant la vérification JWT : `DISABLE_JWT=yes`.

On peut tester les commandes suivantes depuis un terminal (après avoir peuplé la base) :

```bash
curl -X GET -i http://localhost/api/v1/assessed-criteria
curl -X GET -i http://localhost/api/v1/assessed-criteria/1
curl -X GET -i http://localhost/api/v1/assessed-criteria/1000000
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/assessed-criteria --data '{"id_criterion":10,"id_assessment":10,"comment":"un commentaire"}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/assessed-criteria/1 --data '{"comment":"nouveau comm"}'
curl -X DELETE -i http://localhost/api/v1/assessed-criteria/1

curl -X GET -i http://localhost/api/v1/assessments
curl -X GET -i http://localhost/api/v1/assessments?norelated=true
curl -X GET -i http://localhost/api/v1/assessments/1
curl -X GET -i http://localhost/api/v1/assessments/1000000
curl -X GET -i http://localhost/api/v1/assessments/1?norelated=true
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/assessments --data '{"id_production":1,"bonus":2}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/assessments/1 --data '{"id_production":1,"bonus":3}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/assessments/1?norelated=true --data '{"id_production":1,"bonus":4}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/assessments/1 --data '{"score_comment":"com","bonus":5}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/assessments/1?norelated=true --data '{"score_comment":"com 2","bonus":6}'
curl -X DELETE -i http://localhost/api/v1/assessments/1

curl -X GET -i http://localhost/api/v1/criteria-groups
curl -X GET -i http://localhost/api/v1/criteria-groups/1
curl -X GET -i http://localhost/api/v1/criteria-groups/1000000
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria-groups --data '{"position":1,"description":"description a"}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria-groups/1 --data '{"description":"description b"}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria-groups/1 --data '{"relative_weight":3.5}'
curl -X DELETE -i http://localhost/api/v1/criteria-groups/1

curl -X GET -i http://localhost/api/v1/criteria
curl -X GET -i http://localhost/api/v1/criteria?norelated=true
curl -X GET -i http://localhost/api/v1/criteria/1
curl -X GET -i http://localhost/api/v1/criteria/1000000
curl -X GET -i http://localhost/api/v1/criteria/1?norelated=true
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria --data '{"description":"ma nouvelle description","relative_weight":2}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria/1 --data '{"description":"ma nouvelle description a","relative_weight":3}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria/1?norelated=true --data '{"description":"ma nouvelle description b","relative_weight":1.5}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria/1 --data '{"relative_weight":4.87}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/criteria/1?norelated=true --data '{"relative_weight":0.567}'
curl -X DELETE -i http://localhost/api/v1/criteria/1

curl -X GET -i http://localhost/api/v1/descrobs
curl -X GET -i http://localhost/api/v1/descrobs/1
curl -X GET -i http://localhost/api/v1/descrobs/1000000
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/descrobs --data '{"id_criterion":1,"position":1,"description":"description a"}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/descrobs/1 --data '{"id_criterion":1,"position":3}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/descrobs/1 --data '{"value":3.5}'
curl -X DELETE -i http://localhost/api/v1/descrobs/1

curl -X GET -i http://localhost/api/v1/rubrics
curl -X GET -i http://localhost/api/v1/rubrics?norelated=true
curl -X GET -i http://localhost/api/v1/rubrics/1
curl -X GET -i http://localhost/api/v1/rubrics/1000000
curl -X GET -i http://localhost/api/v1/rubrics/1?norelated=true
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/rubrics --data '{"name":"ma nouvelle rubrique","grading":20}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/rubrics/1 --data '{"name":"updated rubrique","grading":21}'
curl -X PUT -H 'Content-Type: application/json' -i http://localhost/api/v1/rubrics/1?norelated=true --data '{"name":"updated rubrique","grading":22}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/rubrics/1 --data '{"grading":23}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/rubrics/1?norelated=true --data '{"grading":24}'
curl -X DELETE -i http://localhost/api/v1/rubrics/1

curl -X GET -i http://localhost/api/v1/selected-descrobs
curl -X GET -i http://localhost/api/v1/selected-descrobs/1
curl -X GET -i http://localhost/api/v1/selected-descrobs/1000000
curl -X POST -H 'Content-Type: application/json' -i http://localhost/api/v1/selected-descrobs --data '{"id_descrob":10,"id_assessment":10}'
curl -X PATCH -H 'Content-Type: application/json' -i http://localhost/api/v1/selected-descrobs/1 --data '{}'
curl -X DELETE -i http://localhost/api/v1/selected-descrobs/1
```

## Avec JWT
1. Modifier le fichier `.env`, habiliter JWT puis renseigner le token correspondant.
2. Récupérer un timestamp tout récent (par exemple sur [https://www.unixtimestamp.com/](https://www.unixtimestamp.com/)).
3. Se rendre sur [https://jwt.io/](https://jwt.io/ ) puis renseigner le payload afin qu'il soit de forme :
```json
{
  "dest": "/v1/activities",
  "iat": 1683549431
}
```
4. Le header doit contenir :
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```
5. Copier le token généré puis, depuis un terminal, lancer la commande suivante en remplaçant `TOKEN` par le token généré :
```bash
curl -X GET -H 'alg: HS256' -H 'typ: JWT' -H 'Authorization: Bearer TOKEN' -i http://localhost/api/v1/activities
```

