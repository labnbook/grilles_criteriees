<?php

namespace App\Console;

use App\Models\CriteriaGroup;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // Delete empty criteria groups that were updated more than one day ago
        $schedule->call(function () {
            CriteriaGroup::doesntHave('criteria')
                ->where('updated_at', '<=', Carbon::now()->subDays())
                ->delete();
        })->dailyAt('6:00');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
