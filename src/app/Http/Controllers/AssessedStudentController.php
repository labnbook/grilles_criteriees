<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssessedStudentResource;
use App\Models\AssessedStudent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AssessedStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $validated_data = $request->validate(['id_assessment' => 'sometimes|nullable|numeric']);
        return AssessedStudentResource::collection(
            AssessedStudent::when(
                array_key_exists('id_assessment', $validated_data),
                function ($query) use ($validated_data) {
                    return $query->where('id_assessment', $validated_data['id_assessment']);
                }
            )
            ->orderBy('id_assessment', 'ASC')
            ->orderBy('id_student', 'ASC')
            ->get()
        );
    }

    /**
     * Display the specified resource.
     * @param  AssessedStudent $assessed_student
     * @return AssessedStudentResource
     */
    public function show(AssessedStudent $assessed_student): AssessedStudentResource
    {
        return new AssessedStudentResource($assessed_student);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $validated_data = $request->validate(AssessedStudent::$validation_rules);
            $assessed_student = AssessedStudent::create($validated_data)->refresh();
            return response()->json(['data' => new AssessedStudentResource($assessed_student)], 201);
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Replace the specified resource in storage.
     * @param Request $request
     * @param AssessedStudent $assessed_student
     * @return JsonResponse
     */
    public function replace(Request $request, AssessedStudent $assessed_student): JsonResponse
    {
        try {
            $validated_data = $request->validate(AssessedStudent::$validation_rules);
            $fillable = app(AssessedStudent::class)->getFillable();
            $assessed_student->update(array_merge(
                array_fill_keys($fillable, null),
                array_fill_keys(AssessedStudent::$false_default_value, 0),
                $validated_data
            ));
            return response()->json(['data' => new AssessedStudentResource($assessed_student)], 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param AssessedStudent $assessed_student
     * @return JsonResponse
     */
    public function update(Request $request, AssessedStudent $assessed_student): JsonResponse
    {
        try {
            $validated_data = $request->validate(AssessedStudent::$validation_rules);
            $assessed_student->update($validated_data);
            return response()->json(['data' => new AssessedStudentResource($assessed_student)], 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  AssessedStudent $assessed_student
     * @return JsonResponse
     */
    public function destroy(AssessedStudent $assessed_student): JsonResponse
    {
        $assessed_student->delete();
        return response()->json(null, 204);
    }
}
