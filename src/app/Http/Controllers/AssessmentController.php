<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssessmentResource;
use App\Models\Assessment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AssessmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $validated_data = $request->validate([
            'id_production' => 'sometimes|nullable|numeric',
            'id_rubric_origin' => 'sometimes|nullable|numeric',
            'id_productions' => 'sometimes|nullable|array',
            'id_productions.*' => 'numeric'
        ]);
        return AssessmentResource::collection(
            Assessment::when(array_key_exists('id_production', $validated_data), function ($query) use ($validated_data) {
                    return $query->where('id_production', $validated_data['id_production']);
            })
                ->when(array_key_exists('id_rubric_origin', $validated_data), function ($query) use ($validated_data) {
                    return $query->where('id_rubric_origin', $validated_data['id_rubric_origin']);
                })
                ->when(array_key_exists('id_productions', $validated_data), function ($query) use ($validated_data) {
                    return $query->whereIn('id_production', $validated_data['id_productions']);
                })
                ->orderBy('id_assessment', 'ASC')
                ->with('criteria', 'criteria.criteria_group', 'criteria.descrobs')
                ->get()
        );
    }

    /**
     * Display the specified resource.
     * @param  Assessment $assessment
     * @return AssessmentResource
     */
    public function show(Assessment $assessment): AssessmentResource
    {
        return new AssessmentResource($assessment);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $validated_data = $request->validate(Assessment::$validation_rules);
            $assessment = Assessment::create($validated_data)->refresh();
            return response()->json(['data' => new AssessmentResource($assessment)], 201);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Replace the specified resource in storage.
     * @param Request $request
     * @param Assessment $assessment
     * @return JsonResponse
     */
    public function replace(Request $request, Assessment $assessment): JsonResponse
    {
        try {
            $validated_data = $request->validate(Assessment::$validation_rules);
            $fillable = app(Assessment::class)->getFillable();
            $assessment->update(array_merge(
                array_fill_keys($fillable, null),
                array_fill_keys(Assessment::$false_default_value, 0),
                $validated_data
            ));
            return response()->json(['data' => new AssessmentResource($assessment)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Assessment $assessment
     * @return JsonResponse
     */
    public function update(Request $request, Assessment $assessment): JsonResponse
    {
        try {
            $validated_data = $request->validate(Assessment::$validation_rules);
            $assessment->update($validated_data);
            return response()->json(['data' => new AssessmentResource($assessment)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  Assessment $assessment
     * @return JsonResponse
     */
    public function destroy(Assessment $assessment): JsonResponse
    {
        $assessment->delete();
        return response()->json(null,204);
    }
}
