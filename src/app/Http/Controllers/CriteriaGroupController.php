<?php

namespace App\Http\Controllers;

use App\Http\Resources\CriteriaGroupResource;
use App\Models\CriteriaGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class CriteriaGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CriteriaGroupResource::collection(
            CriteriaGroup::orderBy('position', 'ASC')
                ->orderBy('id_criteria_group', 'ASC')
                ->get()
        );
    }

    /**
     * Display the specified resource.
     * @param  CriteriaGroup $criteria_group
     * @return CriteriaGroupResource
     */
    public function show(CriteriaGroup $criteria_group): CriteriaGroupResource
    {
        return new CriteriaGroupResource($criteria_group);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $validated_data = $request->validate(CriteriaGroup::$validation_rules);
            $criteria_group = CriteriaGroup::create($validated_data)->refresh();
            return response()->json(['data' => new CriteriaGroupResource($criteria_group)], 201);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Replace the specified resource in storage.
     * @param Request $request
     * @param CriteriaGroup $criteria_group
     * @return JsonResponse
     */
    public function replace(Request $request, CriteriaGroup $criteria_group): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(CriteriaGroup::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $fillable = app(CriteriaGroup::class)->getFillable();
            $zero = CriteriaGroup::$zero_default_value;
            $to_update = array_merge(array_fill_keys($fillable, null), array_fill_keys($zero, 0), $validated_data);
            $criteria_group->update($to_update);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $criteria_group->children
                    ->each(function ($crit_group) use ($to_update) {
                        if ($crit_group->isRelatedToANotPublishedAssessment()) {
                            $crit_group->update($to_update);
                        }
                    });
            }

            return response()->json(['data' => new CriteriaGroupResource($criteria_group)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param CriteriaGroup $criteria_group
     * @return JsonResponse
     */
    public function update(Request $request, CriteriaGroup $criteria_group): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(CriteriaGroup::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $criteria_group->update($validated_data);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $criteria_group->children
                    ->each(function ($crit_group) use ($validated_data) {
                        if ($crit_group->isRelatedToANotPublishedAssessment()) {
                            $crit_group->update($validated_data);
                        }
                    });
            }

            return response()->json(['data' => new CriteriaGroupResource($criteria_group)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param  CriteriaGroup $criteria_group
     * @return JsonResponse
     */
    public function destroy(Request $request, CriteriaGroup $criteria_group): JsonResponse
    {
        $validated_data = $request->validate(['propagate' => 'nullable|numeric']);

        // Delete related objects
        if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
            $criteria_group->children
                ->each(function ($crit_group) {
                    if ($crit_group->isRelatedToANotPublishedAssessment()) {
                        $crit_group->delete();
                    }
                });
        }

        // Delete the object
        $criteria_group->delete();

        return response()->json(null,204);
    }
}
