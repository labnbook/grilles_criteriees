<?php

namespace App\Http\Controllers;

use App\Http\Resources\CriterionResource;
use App\Models\Criterion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class CriterionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CriterionResource::collection(
            Criterion::orderBy('position', 'ASC')
                ->orderBy('id_criterion', 'ASC')
                ->with('criteria_group', 'descrobs')
                ->get()
        );
    }

    /**
     * Display the specified resource.
     * @param  Criterion $criterion
     * @return CriterionResource
     */
    public function show(Criterion $criterion): CriterionResource
    {
        return new CriterionResource($criterion);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Criterion::$validation_rules, ['propagate' => 'nullable|numeric']));
            $criterion = Criterion::create($validated_data)->refresh();
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                if ($criterion->rubric) {
                    $assessments = $criterion->rubric->assessments;
                    foreach ($assessments as $a) {
                        $group = $a->criteria_groups()->where('criteria_group.id_origin', $criterion->id_criteria_group)->first();
                        if (!$group) {
                            // The group does not exist for the assesment, replicate it
                            $group = $criterion->criteria_group->replicate();
                            $group->save();
                        }
                        $new_c = $criterion->replicate(['id_rubric', 'id_criteria_group']);
                        $new_c->id_criteria_group = $group->id_criteria_group;
                        $new_c->id_origin = $criterion->id_criterion;
                        $new_c->id_assessment = $a->id_assessment;
                        $new_c->save();
                    }
                }
            }
            return response()->json(['data' => new CriterionResource($criterion)], 201);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Replace the specified resource in storage.
     * @param Request $request
     * @param Criterion $criterion
     * @return JsonResponse
     */
    public function replace(Request $request, Criterion $criterion): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Criterion::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $fillable = app(Criterion::class)->getFillable();
            $zero = Criterion::$false_default_value;
            $one = Criterion::$true_default_value;
            $descr = Criterion::$descr_default_value;
            $to_update = array_merge(
                array_fill_keys($fillable, null),
                array_fill_keys($zero, 0),
                array_fill_keys($one, 1),
                array_fill_keys($descr, 'descr'),
                $validated_data
            );
            $criterion->update($to_update);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $criterion->children
                    ->each(function ($crit) use ($to_update, $validated_data) {
                        if ($crit->isRelatedToANotPublishedAssessment()) {
                            // The criteria group may need to be updated to match the new related one
                            if (array_key_exists('id_criteria_group', $validated_data)) {
                                $to_update['id_criteria_group'] = Criterion::where('id_rubric', $crit->parent->id_rubric)
                                    ->where('id_criteria_group', $crit->parent->id_criteria_group)
                                    ->whereNot('id_criterion', $crit->parent->id_criterion)
                                    ->first()
                                    // At this step, we have one criterion sibling of the new one (for the rubric)
                                    ->children()
                                    ->where('id_assessment', $crit->id_assessment)
                                    ->whereNot('id_criterion', $crit->id_criterion)
                                    ->first()
                                    // At this step, we have one criterion sibling of the new one (for the assessment)
                                    ->id_criteria_group;
                            }
                            $crit->update($to_update);
                        }
                    });
            }

            return response()->json(['data' => new CriterionResource($criterion)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Criterion $criterion
     * @return JsonResponse
     */
    public function update(Request $request, Criterion $criterion): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Criterion::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $criterion->update($validated_data);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $criterion->children
                    ->each(function ($crit) use ($validated_data) {
                        if ($crit->isRelatedToANotPublishedAssessment()) {
                            // The criteria group may need to be updated to match the new related one
                            if (array_key_exists('id_criteria_group', $validated_data)) {
                                $validated_data['id_criteria_group'] = Criterion::where('id_rubric', $crit->parent->id_rubric)
                                    ->where('id_criteria_group', $crit->parent->id_criteria_group)
                                    ->whereNot('id_criterion', $crit->parent->id_criterion)
                                    ->first()
                                    // At this step, we have one criterion sibling of the new one (for the rubric)
                                    ->children()
                                    ->where('id_assessment', $crit->id_assessment)
                                    ->whereNot('id_criterion', $crit->id_criterion)
                                    ->first()
                                    // At this step, we have one criterion sibling of the new one (for the assessment)
                                    ->id_criteria_group;
                            }
                            $crit->update($validated_data);
                        }
                    });
            }

            return response()->json(['data' => new CriterionResource($criterion)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param  Criterion $criterion
     * @return JsonResponse
     */
    public function destroy(Request $request, Criterion $criterion): JsonResponse
    {
        $validated_data = $request->validate(['propagate' => 'nullable|numeric']);

        // Delete related objects
        if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
            $criterion->children
                ->each(function ($crit) {
                    if ($crit->isRelatedToANotPublishedAssessment()) {
                        $crit->delete();
                    }
                });
        }

        // Delete the object
        $criterion->delete();

        return response()->json(null,204);
    }
}
