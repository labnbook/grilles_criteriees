<?php

namespace App\Http\Controllers;

use App\Http\Resources\DescrobResource;
use App\Models\Criterion;
use App\Models\Descrob;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class DescrobController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return DescrobResource::collection(
            Descrob::orderBy('position', 'ASC')
                ->orderBy('id_descrob', 'ASC')
                ->get()
        );
    }

    /**
     * Display the specified resource.
     * @param  Descrob $descrob
     * @return DescrobResource
     */
    public function show(Descrob $descrob): DescrobResource
    {
        return new DescrobResource($descrob);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Descrob::$validation_rules, ['propagate' => 'nullable|numeric']));
            $descrob = Descrob::create($validated_data)->refresh();
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $criterion = $descrob->criterion;
                if ($criterion->rubric) {
                    $assessments = $descrob->criterion->rubric->assessments;
                    foreach ($assessments as $a) {
                        $new_d = $descrob->replicate(['id_criterion', 'id_origin']);
                        $c = Criterion::where('id_assessment', $a->id_assessment)
                            ->where('id_origin', $criterion->id_criterion)
                            ->first();
                        $new_d->id_origin = $descrob->id_descrob;
                        $new_d->id_criterion = $c->id_criterion;
                        $new_d->save();
                    }
                }
            }
            return response()->json(['data' => new DescrobResource($descrob)], 201);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Replace the specified resource in storage.
     * @param Request $request
     * @param Descrob $descrob
     * @return JsonResponse
     */
    public function replace(Request $request, Descrob $descrob): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Descrob::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $fillable = app(Descrob::class)->getFillable();
            $zero = array_merge(Descrob::$false_default_value, Descrob::$zero_default_value);
            $to_update = array_merge(array_fill_keys($fillable, null), array_fill_keys($zero, 0), $validated_data);
            $descrob->update($to_update);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $descrob->children
                    ->each(function ($des) use ($to_update, $validated_data) {
                        if ($des->criterion->isRelatedToANotPublishedAssessment()) {
                            // The criterion may need to be updated to match the new related one
                            if (array_key_exists('id_criterion', $validated_data)) {
                                $to_update['id_criterion'] = Criterion::find($des->parent->id_criterion)
                                    // At this step, we have the new criterion of the rubric
                                    ->children()
                                    ->where('id_assessment', $des->criterion->id_assessment)
                                    // Assuming there is only one child of a rubric criterion with a given id_assessment
                                    ->first()
                                    ->id_criterion;
                            }
                            $des->update($to_update);
                        }
                    });
            }

            return response()->json(['data' => new DescrobResource($descrob)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Descrob $descrob
     * @return JsonResponse
     */
    public function update(Request $request, Descrob $descrob): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Descrob::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $descrob->update($validated_data);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $descrob->children
                    ->each(function ($des) use ($validated_data) {
                        if ($des->criterion->isRelatedToANotPublishedAssessment()) {
                            // The criterion may need to be updated to match the new related one
                            if (array_key_exists('id_criterion', $validated_data)) {
                                $validated_data['id_criterion'] = Criterion::find($des->parent->id_criterion)
                                    // At this step, we have the new criterion of the rubric
                                    ->children()
                                    ->where('id_assessment', $des->criterion->id_assessment)
                                    // Assuming there is only one child of a rubric criterion with a given id_assessment
                                    ->first()
                                    ->id_criterion;
                            }
                            $des->update($validated_data);
                        }
                    });
            }

            return response()->json(['data' => new DescrobResource($descrob)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param  Descrob $descrob
     * @return JsonResponse
     */
    public function destroy(Request $request, Descrob $descrob): JsonResponse
    {
        $validated_data = $request->validate(['propagate' => 'nullable|numeric']);

        // Delete related objects
        if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
            $descrob->children
                ->each(function ($des) {
                    if ($des->criterion->isRelatedToANotPublishedAssessment()) {
                        $des->delete();
                    }
                });
        }

        // Delete the object
        $descrob->delete();
        return response()->json(null,204);
    }
}
