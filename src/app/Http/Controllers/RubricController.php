<?php

namespace App\Http\Controllers;

use App\Http\Resources\RubricResource;
use App\Models\Assessment;
use App\Models\Rubric;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class RubricController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $validated_data = $request->validate([
            'id_activity' => 'sometimes|nullable|numeric',
            'id_activities' => 'sometimes|nullable|array',
            'id_activities.*' => 'sometimes|nullable|numeric'
        ]);
        return RubricResource::collection(
            Rubric::when(array_key_exists('id_activity', $validated_data), function($query) use ($validated_data) {
                    return $query->where('id_activity', $validated_data['id_activity']);
                })->when(array_key_exists('id_activities', $validated_data), function($query) use ($validated_data) {
                    return $query->whereIn('id_activity', $validated_data['id_activities']);
                })
                ->orderBy('id_rubric', 'ASC')
                ->with('criteria', 'criteria.criteria_group', 'criteria.descrobs')
                ->get()
        );
    }

    /**
     * Display the specified resource.
     * @param  Rubric $rubric
     * @return RubricResource
     */
    public function show(Rubric $rubric): RubricResource
    {
        return new RubricResource($rubric);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $validated_data = $request->validate(Rubric::$validation_rules);
            $rubric = Rubric::create($validated_data)->refresh();
            return response()->json(['data' => new RubricResource($rubric)], 201);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Replace the specified resource in storage.
     * @param Request $request
     * @param Rubric $rubric
     * @return JsonResponse
     */
    public function replace(Request $request, Rubric $rubric): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Rubric::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $fillable = app(Rubric::class)->getFillable();
            $to_update = array_merge(array_fill_keys($fillable, null), $validated_data);
            $rubric->update($to_update);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $rubric->assessments->where('published', 0)
                    ->each(function ($assessment) use ($to_update) {
                        $assessment->update($to_update);
                    });
            }

            return response()->json(['data' => new RubricResource($rubric)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Rubric $rubric
     * @return JsonResponse
     */
    public function update(Request $request, Rubric $rubric): JsonResponse
    {
        try {
            $validated_data = $request->validate(array_merge(Rubric::$validation_rules, ['propagate' => 'nullable|numeric']));

            // Update the object
            $rubric->update($validated_data);

            // Update related objects
            if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
                $rubric->assessments->where('published', 0)
                    ->each(function ($assessment) use ($validated_data) {
                        $assessment->update($validated_data);
                    });
            }

            return response()->json(['data' => new RubricResource($rubric)], 200);
        } catch (\Illuminate\Database\QueryException $exception){
            return response()->json(['error' => $exception], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param  Rubric $rubric
     * @return JsonResponse
     */
    public function destroy(Request $request, Rubric $rubric): JsonResponse
    {
        $validated_data = $request->validate(['propagate' => 'nullable|numeric']);

        // Delete related objects
        if (array_key_exists('propagate', $validated_data) && $validated_data['propagate'] == 1) {
            $rubric->assessments->where('published', 0)
                ->each(function ($assessment) {
                    $assessment->delete();
                });
        }

        // Delete the object
        $rubric->delete();

        return response()->json(null,204);
    }
}
