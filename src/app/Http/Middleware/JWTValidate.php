<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class JWTValidate
{

    /** @var array fields that MUST be in all jwt */
    private $requiredFields = ['iat'=>0, 'dest'=>0];

    /** @var int ttl seconds */
    private $ttl;

    /** @var int leeway seconds (clock skew)*/
    private $leeway;

    public function  __construct(){
        $this->ttl = env('TOKEN_TTL', 600);
        $this->leeway = env('TOKEN_LEEWAY', 20);
    }

    /**
     * Send a json error message
     * @param int code
     * @param string message
     */
    private function fail($code, $message){
        return response()->json(['message'=>$message],$code);
    }

    /**
     * Configures the JWT parser
     */
    private function getJWTConfiguration($key)
    {
        return Configuration::forSymmetricSigner(
            new Sha256(),
            InMemory::plainText($key)
        );
    }

    /**
     * Validate JWT payload and returns it
     * @return JWT payload
     */
    private function validateJWT($token, $path){
        # First parser we do not know who is talking yet so we do not
        # know which key to use, we provide 'nokey' argument to avoid errors
        $config = $this->getJWTConfiguration('nokey');

        $jwt = $config->parser()->parse($token);
        $payload = $jwt->claims()->all();
        if(array_key_exists('data', $payload)){
            $payload['data'] = (array)$payload['data'];
        }else{
            $payload['data'] = [];
        }

        // Check all required fields are present
        $missings = array_diff_key($this->requiredFields, $payload);
        if(!empty($missings)){
            throw new JWTException("Missing required field(s) ".
                json_encode(array_keys($missings)));
        }

        $now =\Carbon\Carbon::now()->getTimestamp();
        // Check that token is not expired
        $iat = new \Carbon\Carbon($payload['iat']);
        $expireTms = $iat->addSeconds($this->ttl)->getTimestamp();
        if ($now > $expireTms) {
            throw new JWTException('Token expired');
        }
        $iat = new \Carbon\Carbon($payload['iat']);
        if ($iat->getTimestamp() > $now) {
            throw new JWTException('Token emitted in the future');
        }

        // Check payload path is allowed
        if (ltrim($payload['dest'], '/') !== ltrim($path, '/')) {
            throw new JWTException("Token not authorized for the path $path");
        }

        // Retrieve secret
        $secret = env('SECRET_JWT', '');
        if(!$secret){
            throw new JWTException('Cannot retrieve secret key.');
        }

        // Verify signature
        $config = $this->getJWTConfiguration($secret);
        $jwt = $config->parser()->parse($token);
        $config->setValidationConstraints(
            new SignedWith($config->signer(), $config->verificationKey()),
        );
        $constraints = $config->validationConstraints();
        if (!$config->validator()->validate($jwt, ...$constraints)) {
            throw new JWTException('Bad signature');
        }
        return $payload;
    }

    /**
     * Handle a request :
     *  1. Validate JWT
     *      1. Authenticate source server
     *      2. Validate data signature
     *  2. Replace request's data with trusted JWT payload
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return  mixed
     */
    public function handle($request, Closure $next)
    {
        // Validate and retrieve JWT Payload
        try {
            $token = $request->bearerToken();
            if ($token == null) {
                $token = $request->input('token', '');
            }
            $payload = $this->validateJWT($token, $request->path());
        } catch (JWTException $e) {
            Log::Warning('JWT Exception while authenticating via API: "'.$e->getMessage().'"');
            return $this->fail(401, $e->getMessage());
        } catch (\InvalidArgumentException $e) {
            Log::Error('Error validating token while authenticating via API: "'.$e->getMessage().'"');
            return $this->fail(401, 'Missing or corrupted token: '.$e->getMessage());
        }

        $request->replace($payload['data']);

        return $next($request);
    }
}

class JWTException extends \Exception {};
