<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AssessedStudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_assessed_student' => $this->id_assessed_student,
            'id_student' => $this->id_student,
            'id_assessment' => $this->id_assessment,
            'missing' => $this->missing,
        ];
    }
}
