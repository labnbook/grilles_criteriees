<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AssessmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_assessment' => $this->id_assessment,
            'id_production' => $this->id_production,
            'id_rubric_origin' => $this->id_rubric_origin,
            'grading' => $this->grading,
            'min_grade' => $this->min_grade,
            'max_grade' => $this->max_grade,
            'min_bonus' => $this->min_bonus,
            'max_bonus' => $this->max_bonus,
            'bonus' => $this->bonus,
            'bonus_comment' => $this->bonus_comment,
            'score' => $this->score,
            'score_comment' => $this->score_comment,
            'published' => $this->published,
            'criteria' => $this->when(
                $request->input('norelated') !== 'true',
                CriterionResource::collection($this->criteria)
            )
        ];
    }
}
