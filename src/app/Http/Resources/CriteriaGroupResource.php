<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CriteriaGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_criteria_group' => $this->id_criteria_group,
            'id_origin' => $this->id_origin,
            'position' => $this->position,
            'description' => $this->description,
            'relative_weight' => $this->relative_weight
        ];
    }
}
