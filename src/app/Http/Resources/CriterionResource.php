<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CriterionResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_criterion' => $this->id_criterion,
            'id_origin' => $this->id_origin,
            'id_rubric' => $this->id_rubric,
            'id_assessment' => $this->id_assessment,
            'id_section' => $this->id_section,
            'id_criteria_group' => $this->id_criteria_group,
            'type' => $this->type,
            'position' => $this->position,
            'description' => $this->description,
            'relative_weight' => $this->relative_weight,
            'comment' => $this->comment,
            'activated' => $this->activated,
            'criteria_group' => $this->when(
                $request->input('norelated') !== 'true',
                new CriteriaGroupResource($this->criteria_group)
            ),
            'descrobs' => $this->when(
                $request->input('norelated') !== 'true',
                DescrobResource::collection($this->descrobs)
            )
        ];
    }
}
