<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DescrobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_descrob' => $this->id_descrob,
            'id_origin' => $this->id_origin,
            'id_criterion' => $this->id_criterion,
            'description' => $this->description,
            'value' => $this->value,
            'position' => $this->position,
            'selected' => $this->selected
        ];
    }
}
