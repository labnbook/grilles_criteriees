<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RubricResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id_rubric' => $this->id_rubric,
            'id_activity' => $this->id_activity,
            'name' => $this->name,
            'grading' => $this->grading,
            'min_grade' => $this->min_grade,
            'max_grade' => $this->max_grade,
            'min_bonus' => $this->min_bonus,
            'max_bonus' => $this->max_bonus,
            'criteria' => $this->when(
                $request->input('norelated') !== 'true',
                CriterionResource::collection($this->criteria)
            )
        ];
    }
}
