<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AssessedStudent extends Model
{
    use HasFactory;

    protected $table = 'assessed_student';
    protected $primaryKey = 'id_assessed_student';
    protected $fillable = ['id_student', 'id_assessment', 'missing'];
    public static array $false_default_value = ['missing'];
    public static array $validation_rules = [
        'id_student' => 'numeric',
        'id_assessment' => 'numeric',
        'missing' => 'nullable|boolean',
    ];

    /**
     * Get the assessment related to an assessed_student.
     */
    public function assessment(): BelongsTo
    {
        return $this->belongsTo(Assessment::class, 'id_assessment', 'id_assessment');
    }
}
