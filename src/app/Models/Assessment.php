<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Assessment extends Model
{
    use HasFactory;

    protected $table = 'assessment';
    protected $primaryKey = 'id_assessment';
    protected $fillable = ['id_production', 'id_rubric_origin', 'bonus', 'bonus_comment', 'score', 'score_comment',
        'published', 'grading', 'min_grade', 'max_grade', 'min_bonus', 'max_bonus'];
    public static array $false_default_value = ['published'];
    public static array $validation_rules = [
        'id_production' => 'nullable|numeric',
        'id_rubric_origin' => 'nullable|numeric',
        'bonus' => 'nullable|numeric',
        'bonus_comment' => 'nullable|string',
        'score' => 'nullable|numeric',
        'score_comment' => 'nullable|string',
        'published' => 'nullable|boolean',
        'grading' => 'nullable|numeric',
        'min_grade' => 'nullable|numeric',
        'max_grade' => 'nullable|numeric',
        'min_bonus' => 'nullable|numeric',
        'max_bonus' => 'nullable|numeric'
    ];

    /**
     * Get the rubric related to an assessment.
     */
        public function rubric(): BelongsTo
    {
        return $this->belongsTo(Rubric::class, 'id_rubric_origin', 'id_rubric');
    }

    /**
     * Get the criteria related to an assessment.
     */
    public function criteria(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_assessment')
            ->orderby('criterion.position', 'ASC');
    }

    /**
     * Get the criteria_group of an assessment
     */
    public function criteria_groups(): belongsToMany
    {
        return $this->belongsToMany(
            CriteriaGroup::class,
            'criterion',
            'id_assessment',
            'id_criteria_group'
        )->distinct('id_criteria_group');
    }

    /**
     * Get the assessed students related to an assessment.
     */
    public function assessedStudents(): HasMany
    {
        return $this->hasMany(AssessedStudent::class, 'id_assessment')
            ->orderby('assessed_student.id_student', 'ASC');
    }
}
