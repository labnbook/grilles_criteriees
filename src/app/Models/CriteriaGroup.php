<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;

class CriteriaGroup extends Model
{
    use HasFactory;

    protected $table = 'criteria_group';
    protected $primaryKey = 'id_criteria_group';
    protected $fillable = ['id_origin', 'position', 'description', 'relative_weight'];
    public static array $zero_default_value = ['position'];
    public static array $validation_rules = [
        'id_origin' => 'nullable|numeric',
        'position' => 'nullable|numeric',
        'description' => 'nullable|string',
        'relative_weight' => 'nullable|numeric'
    ];

    /**
     * Get the criteria related to a criteria group.
     */
    public function criteria(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_criteria_group')
            ->orderby('criterion.id_criterion', 'ASC');
    }

    /**
     * Get the parent criteria_group related to a criteria_group.
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(CriteriaGroup::class, 'id_origin', 'id_criteria_group');
    }

    /**
     * Get the children criteria_groups related to a criteria_group.
     */
    public function children(): HasMany
    {
        return $this->hasMany(CriteriaGroup::class, 'id_origin', 'id_criteria_group')
            ->orderBy('id_criteria_group', 'ASC');
    }

    /**
     * Check if at least one criterion related to this criteria_group is related to a not published assessment
     * Used in case of propagation request
     * @return bool: iff true, the criteria_group can be updated/deleted
     */
    public function isRelatedToANotPublishedAssessment(): bool
    {
        $updatable = true;
        $this->criteria->each(function ($criterion) use (&$updatable) {
            if (!$criterion->isRelatedToANotPublishedAssessment()) {
                $updatable = false;
                return false;
            }
        });
        return $updatable;
    }
}
