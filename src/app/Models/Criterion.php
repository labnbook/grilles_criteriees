<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;

class Criterion extends Model
{
    use HasFactory;

    protected $table = 'criterion';
    protected $primaryKey = 'id_criterion';
    protected $fillable = ['id_origin', 'id_rubric', 'id_assessment', 'id_section', 'id_criteria_group', 'type',
        'position', 'description', 'relative_weight', 'comment', 'activated'];
    public static array $false_default_value = ['position'];
    public static array $true_default_value = ['activated'];
    public static array $descr_default_value = ['type'];
    public static array $validation_rules = [
        'id_origin' => 'nullable|numeric',
        'id_rubric' => 'nullable|numeric',
        'id_assessment' => 'nullable|numeric',
        'id_section' => 'nullable|numeric',
        'id_criteria_group' => 'nullable|numeric',
        'type' => 'nullable|string',
        'position' => 'nullable|numeric',
        'description' => 'nullable|string',
        'relative_weight' => 'nullable|numeric',
        'comment' => 'nullable|string',
        'activated' => 'nullable|boolean'
    ];

    /**
     * Get the descrobs related to a criterion.
     */
    public function descrobs(): HasMany
    {
        return $this->hasMany(Descrob::class, 'id_criterion', 'id_criterion')
            ->orderBy('position', 'ASC')
            ->orderBy('id_descrob', 'ASC');
    }

    /**
     * Get the criteria_group related to a criterion.
     */
    public function criteria_group(): BelongsTo
    {
        return $this->belongsTo(CriteriaGroup::class, 'id_criteria_group', 'id_criteria_group');
    }

    /**
     * Get the assessment related to a criterion.
     */
    public function assessment(): BelongsTo
    {
        return $this->belongsTo(Assessment::class, 'id_assessment', 'id_assessment');
    }

    /**
     * Get the rubric related to a criterion.
     */
    public function rubric(): BelongsTo
    {
        return $this->belongsTo(Rubric::class, 'id_rubric', 'id_rubric');
    }

    /**
     * Get the parent criterion related to a criterion.
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Criterion::class, 'id_origin', 'id_criterion');
    }

    /**
     * Get the children criteria related to a criterion.
     */
    public function children(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_origin', 'id_criterion')
            ->orderBy('id_criterion', 'ASC');
    }

    /**
     * Check if the criterion is related to a not published assessment
     * Used in case of propagation request
     * @return bool: iff true, the criterion can be updated/deleted
     */
    public function isRelatedToANotPublishedAssessment(): bool
    {
        return $this->assessment === null || $this->assessment->published == 0;
    }
}
