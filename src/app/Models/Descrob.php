<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Descrob extends Model
{
    use HasFactory;

    protected $table = 'descrob';
    protected $primaryKey = 'id_descrob';
    protected $fillable = ['id_origin', 'id_criterion', 'description', 'value', 'position', 'selected'];
    public static array $zero_default_value = ['position'];
    public static array $false_default_value = ['selected'];
    public static array $validation_rules = [
        'id_origin' => 'nullable|numeric',
        'id_criterion' => 'nullable|numeric',
        'description' => 'nullable|string',
        'value' => 'nullable|numeric',
        'position' => 'nullable|numeric',
        'selected' => 'nullable|boolean'
    ];

    /**
     * Get the criterion related to a descrob.
     */
    public function criterion(): BelongsTo
    {
        return $this->belongsTo(Criterion::class, 'id_criterion', 'id_criterion');
    }

    /**
     * Get the parent descrob related to a descrob.
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Descrob::class, 'id_origin', 'id_descrob');
    }

    /**
     * Get the children descrobs related to a descrob.
     */
    public function children(): HasMany
    {
        return $this->hasMany(Descrob::class, 'id_origin', 'id_descrob')
            ->orderBy('id_descrob', 'ASC');
    }
}
