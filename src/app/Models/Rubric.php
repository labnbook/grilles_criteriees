<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Rubric extends Model
{
    use HasFactory;

    protected $table = 'rubric';
    protected $primaryKey = 'id_rubric';
    protected $fillable = ['id_activity', 'name', 'grading', 'min_grade', 'max_grade', 'min_bonus', 'max_bonus'];
    public static array $validation_rules = [
        'id_activity' => 'nullable|numeric',
        'name' => 'nullable|string',
        'grading' => 'nullable|numeric',
        'min_grade' => 'nullable|numeric',
        'max_grade' => 'nullable|numeric',
        'min_bonus' => 'nullable|numeric',
        'max_bonus' => 'nullable|numeric'
    ];

    /**
     * Get the assessments related to a rubric.
     */
    public function assessments(): HasMany
    {
        return $this->hasMany(Assessment::class, 'id_rubric_origin', 'id_rubric')
            ->orderBy('id_assessment', 'ASC');
    }

    /**
     * Get the criteria related to a rubric.
     */
    public function criteria(): HasMany
    {
        return $this->hasMany(Criterion::class, 'id_rubric', 'id_rubric')
            ->orderBy('position', 'ASC')
            ->orderBy('id_criterion', 'ASC');
    }
}
