<?php

namespace Database\Factories;

use App\Models\Assessment;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AssessedStudent>
 */
class AssessedStudentFactory extends Factory
{
    private int $student_id = 0;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $assessment = Assessment::all()->random();
        return [
            'id_assessment' => $assessment->id_assessment,
            'id_student' => $this->student_id++,
            'missing' => !$this->faker->boolean(75)
        ];
    }
}
