<?php

namespace Database\Factories;

use App\Models\Rubric;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Assessment>
 */
class AssessmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $rubric = Rubric::all()->random();
        return [
            'id_production' => $this->faker->numberBetween(1, 5),
            'id_rubric_origin' => $rubric->id_rubric,
            'bonus' => $this->faker->numberBetween($rubric->min_bonus, $rubric->max_bonus),
            'bonus_comment' => $this->faker->sentence(),
            'score' => $this->faker->numberBetween($rubric->min_grade, $rubric->max_grade),
            'score_comment' => $this->faker->sentence(),
            'published' => $this->faker->boolean(),
            'grading' => $this->faker->numberBetween(1, 99),
            'min_grade' => $this->faker->numberBetween(0, 20),
            'max_grade' => $this->faker->numberBetween(80, 100),
            'min_bonus' => $this->faker->numberBetween(-10, 1),
            'max_bonus' => $this->faker->numberBetween(2, 10)
        ];
    }
}
