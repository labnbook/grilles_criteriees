<?php

namespace Database\Factories;

use App\Models\CriteriaGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CriteriaGroup>
 */
class CriteriaGroupFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $all_criteria_groups = CriteriaGroup::all();
        if ($all_criteria_groups->isEmpty()){
            $id_origin = null;
            $position = 0;
        } else {
            $id_origin = $all_criteria_groups->where('id_origin', null)->random()->id_criteria_group;
            $position = $all_criteria_groups->sortByDesc('position')->first()->position + 1;
        }
        return [
            'id_origin' => $this->faker->boolean(75) ? $id_origin : null,
            'position' => $position,
            'description' => $this->faker->sentence(),
            'relative_weight' => $this->faker->randomFloat(2, 1, 10)
        ];
    }
}
