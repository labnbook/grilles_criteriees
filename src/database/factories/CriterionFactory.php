<?php

namespace Database\Factories;

use App\Models\Assessment;
use App\Models\CriteriaGroup;
use App\Models\Criterion;
use App\Models\Rubric;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Criterion>
 */
class CriterionFactory extends Factory
{
    /**
     * Define the model's default state.
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $id_rubric = Rubric::all()->random()->id_rubric;
        $id_assessment = Assessment::all()->random()->id_assessment;
        $is_assessment = $this->faker->boolean(75);

        $id_criteria_group = CriteriaGroup::all()->random()->id_criteria_group;
        $all_criteria = Criterion::all();
        $all_criteria_for_rubric = $all_criteria->where('id_rubric', $id_rubric);

        // Set position
        if ($all_criteria_for_rubric->isEmpty()){
            $position = 0;
        } else {
            $position = $all_criteria_for_rubric->sortByDesc('position')->first()->position + 1;
        }

        // Set id_origin
        if ($all_criteria->isEmpty() || $all_criteria->where('id_origin', null)->where('id_assessment', null)->isEmpty()){
            $id_origin = null;
        } else {
            $id_origin = $all_criteria->where('id_origin', null)->where('id_assessment', null)->random()->id_criterion;
        }
        return [
            'id_origin' => $this->faker->boolean(50) ? $id_origin : null,
            'id_rubric' => $is_assessment ? null : $id_rubric,
            'id_assessment' => $is_assessment ? $id_assessment : null,
            'id_section' => $this->faker->numberBetween(1, 5),
            'id_criteria_group' => $this->faker->boolean(75) ? $id_criteria_group : null,
            'type' => $this->faker->randomElement(['descr', 'ob']),
            'position' => $position,
            'description' => $this->faker->sentence(),
            'relative_weight' => $this->faker->randomFloat(2, 1, 10),
            'comment' => $is_assessment ? $this->faker->sentence() : null,
            'activated' => !$is_assessment || $this->faker->boolean(80)
        ];
    }
}
