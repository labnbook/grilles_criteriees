<?php

namespace Database\Factories;

use App\Models\Criterion;
use App\Models\Descrob;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Descrob>
 */
class DescrobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $id_criterion = Criterion::all()->random()->id_criterion;
        $all_descrobs = Descrob::all();
        $all_descrobs_for_criterion = $all_descrobs->where('id_criterion', $id_criterion);
        // Set position
        if ($all_descrobs_for_criterion->isEmpty()){
            $position = 0;
        } else {
            $position = $all_descrobs_for_criterion->sortByDesc('position')->first()->position + 1;
        }
        // Set id_origin
        if ($all_descrobs->isEmpty()){
            $id_origin = null;
        } else {
            $id_origin = $all_descrobs->where('id_origin', null)->random()->id_descrob;
        }
        return [
            'id_origin' => $this->faker->boolean(75) ? $id_origin : null,
            'id_criterion' => $id_criterion,
            'description' => $this->faker->sentence(),
            'value' => $this->faker->numberBetween(0, 1) + $position,
            'position' => $position,
            'selected' => $this->faker->boolean(75)
        ];
    }
}
