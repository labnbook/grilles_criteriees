<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rubric>
 */
class RubricFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id_activity' => $this->faker->numberBetween(1, 5),
            'name' => $this->faker->words($nb = 3, $asText = true),
            'grading' => $this->faker->numberBetween(1, 99),
            'min_grade' => $this->faker->numberBetween(0, 20),
            'max_grade' => $this->faker->numberBetween(80, 100),
            'min_bonus' => $this->faker->numberBetween(-10, 1),
            'max_bonus' => $this->faker->numberBetween(2, 10)
        ];
    }
}
