<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('criteria_group', function (Blueprint $table) {
            $table->id('id_criteria_group')->nullable(false);
            $table->unsignedBigInteger('id_origin')->nullable();
            $table->smallInteger('position')->nullable(false)->default(0);
            $table->longText('description')->nullable();
            $table->float('relative_weight', 8, 2)->nullable();
            $table->timestampsTz($precision = 0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });

        Schema::table('criteria_group', function (Blueprint $table) {
            $table->foreign('id_origin')->references('id_criteria_group')->on('criteria_group')->cascadeOnUpdate()->nullOnDelete();;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('criteria_group');
        Schema::enableForeignKeyConstraints();
    }
};
