<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rubric', function (Blueprint $table) {
            $table->id('id_rubric')->nullable(false);
            $table->unsignedBigInteger('id_activity')->nullable();
            $table->char('name', 255)->nullable();
            $table->float('grading', 8, 2)->nullable();
            $table->float('min_grade', 8, 2)->nullable();
            $table->float('max_grade', 8, 2)->nullable();
            $table->float('min_bonus', 8, 2)->nullable();
            $table->float('max_bonus', 8, 2)->nullable();
            $table->timestampsTz($precision = 0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rubric');
        Schema::enableForeignKeyConstraints();
    }
};
