<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('criterion', function (Blueprint $table) {
            $table->id('id_criterion')->nullable(false);
            $table->unsignedBigInteger('id_origin')->nullable();
            $table->unsignedBigInteger('id_rubric')->nullable(); // if a rubric is deleted, the associated criteria still remain
            $table->unsignedBigInteger('id_assessment')->nullable(); // if an assessment is deleted, the associated criteria still remain
            $table->unsignedBigInteger('id_section')->nullable();
            $table->unsignedBigInteger('id_criteria_group')->nullable(); // if a criteria_group is deleted, the associated criteria still remain
            $table->foreign('id_rubric')->references('id_rubric')->on('rubric')->cascadeOnUpdate()->nullOnDelete();
            $table->foreign('id_assessment')->references('id_assessment')->on('assessment')->cascadeOnUpdate()->nullOnDelete();
            $table->foreign('id_criteria_group')->references('id_criteria_group')->on('criteria_group')->cascadeOnUpdate()->nullOnDelete();
            $table->enum('type', ['descr', 'ob'])->nullable(false)->default('descr');
            $table->smallInteger('position')->nullable(false)->default(0);
            $table->longText('description')->nullable();
            $table->float('relative_weight', 8, 2)->nullable();
            $table->longText('comment')->nullable();
            $table->boolean('activated')->nullable(false)->default(true);
            $table->timestampsTz($precision = 0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });

        Schema::table('criterion', function (Blueprint $table) {
            $table->foreign('id_origin')->references('id_criterion')->on('criterion')->cascadeOnUpdate()->nullOnDelete();;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('criterion');
        Schema::enableForeignKeyConstraints();
    }
};
