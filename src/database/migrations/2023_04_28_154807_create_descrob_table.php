<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('descrob', function (Blueprint $table) {
            $table->id('id_descrob')->nullable(false);
            $table->unsignedBigInteger('id_origin')->nullable();
            $table->unsignedBigInteger('id_criterion')->nullable();
            $table->foreign('id_criterion')->references('id_criterion')->on('criterion')->cascadeOnUpdate()->cascadeOnDelete();
            $table->longText('description')->nullable();
            $table->float('value', 8, 2)->nullable();
            $table->smallInteger('position')->nullable(false)->default(0);
            $table->boolean('selected')->nullable(false)->default(false);
            $table->timestampsTz($precision = 0);
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        });

        Schema::table('descrob', function (Blueprint $table) {
            $table->foreign('id_origin')->references('id_descrob')->on('descrob')->cascadeOnUpdate()->nullOnDelete();;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('descrob');
        Schema::enableForeignKeyConstraints();
    }
};
