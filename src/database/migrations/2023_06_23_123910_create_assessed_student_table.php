<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assessed_student', function (Blueprint $table) {
            $table->id('id_assessed_student');
            $table->unsignedBigInteger('id_assessment')->nullable(false);
            $table->unsignedBigInteger('id_student')->nullable(false);
            $table->foreign('id_assessment')
                  ->references('id_assessment')
                  ->on('assessment')
                  ->cascadeOnUpdate()
                  ->cascadeOnDelete();
            $table->boolean('missing')->nullable(false)->default(false);
            $table->unique(['id_student', 'id_assessment']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('assessed_student');
        Schema::enableForeignKeyConstraints();
    }
};
