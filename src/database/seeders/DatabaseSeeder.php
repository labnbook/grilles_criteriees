<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\AssessedCriterion;
use App\Models\AssessedStudent;
use App\Models\Assessment;
use App\Models\CriteriaGroup;
use App\Models\Criterion;
use App\Models\Descrob;
use App\Models\Rubric;
use App\Models\SelectedDescrob;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Faker\Factory;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $faker = Factory::create();
        $faker->seed(env('SEEDER_SEED'));

        //Schema::disableForeignKeyConstraints();

        $n_criteria_groups = 2;
        for ($idx=0; $idx<$n_criteria_groups; $idx++) {
            CriteriaGroup::factory()->create();
        }
        //error_log('$n_criteria_groups: '.$n_criteria_groups);

        $n_rubrics = 4;
        Rubric::factory($n_rubrics)->create();
        //error_log('$n_rubrics: '.$n_rubrics);

        $n_assessments = 8;
        Assessment::factory($n_assessments)->create();
        //error_log('$n_assessments: '.$n_assessments);

        $n_criteria = floor(3 * $n_rubrics);
        for ($idx=0; $idx<$n_criteria; $idx++) {
            Criterion::factory()->create();
        }
        //error_log('$n_criteria: '.$n_criteria);

        $n_descrobs = $n_criteria * 5;
        for ($idx=0; $idx<$n_descrobs; $idx++) {
            Descrob::factory()->create();
        }
        //error_log('$n_descrobs: '.$n_descrobs);

        $n_assessed_students = floor($n_assessments * 2.5);
        AssessedStudent::factory($n_assessed_students)->create();
        //error_log('$n_assessed_students: '.$n_assessed_students);

        //Schema::enableForeignKeyConstraints();
    }
}
