<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CriteriaGroupController;
use App\Http\Controllers\RubricController;
use App\Http\Controllers\AssessmentController;
use App\Http\Controllers\CriterionController;
Use App\Http\Controllers\DescrobController;
Use App\Http\Controllers\AssessedCriterionController;
Use App\Http\Controllers\AssessedStudentController;
Use App\Http\Controllers\SelectedDescrobController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

$middleware = (env('DISABLE_JWT', 'no') === 'yes') ? [] : ['jwtvalidate'];

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1/',
    'middleware' => $middleware,
], function () {
    Route::get('assessed-students', [AssessedStudentController::class, 'index'])->name('assessed-student.index');
    Route::get('assessed-students/{assessed_student}', [AssessedStudentController::class, 'show'])->name('assessed-student.show');
    Route::post('assessed-students', [AssessedStudentController::class, 'store'])->name('assessed-student.store');
    Route::put('assessed-students/{assessed_student}', [AssessedStudentController::class, 'replace'])->name('assessed-student.replace');
    Route::patch('assessed-students/{assessed_student}', [AssessedStudentController::class, 'update'])->name('assessed-student.update');
    Route::delete('assessed-students/{assessed_student}', [AssessedStudentController::class, 'destroy'])->name('assessed-student.destroy');

    Route::get('assessments', [AssessmentController::class, 'index'])->name('assessment.index');
    Route::get('assessments/{assessment}', [AssessmentController::class, 'show'])->name('assessment.show');
    Route::post('assessments', [AssessmentController::class, 'store'])->name('assessment.store');
    Route::put('assessments/{assessment}', [AssessmentController::class, 'replace'])->name('assessment.replace');
    Route::patch('assessments/{assessment}', [AssessmentController::class, 'update'])->name('assessment.update');
    Route::delete('assessments/{assessment}', [AssessmentController::class, 'destroy'])->name('assessment.destroy');

    Route::get('criteria-groups', [CriteriaGroupController::class, 'index'])->name('criteria-group.index');
    Route::get('criteria-groups/{criteria_group}', [CriteriaGroupController::class, 'show'])->name('criteria-group.show');
    Route::post('criteria-groups', [CriteriaGroupController::class, 'store'])->name('criteria-group.store');
    Route::put('criteria-groups/{criteria_group}', [CriteriaGroupController::class, 'replace'])->name('criteria-group.replace');
    Route::patch('criteria-groups/{criteria_group}', [CriteriaGroupController::class, 'update'])->name('criteria-group.update');
    Route::delete('criteria-groups/{criteria_group}', [CriteriaGroupController::class, 'destroy'])->name('criteria-group.destroy');

    Route::get('criteria', [CriterionController::class, 'index'])->name('criterion.index');
    Route::get('criteria/{criterion}', [CriterionController::class, 'show'])->name('criterion.show');
    Route::post('criteria', [CriterionController::class, 'store'])->name('criterion.store');
    Route::put('criteria/{criterion}', [CriterionController::class, 'replace'])->name('criterion.replace');
    Route::patch('criteria/{criterion}', [CriterionController::class, 'update'])->name('criterion.update');
    Route::delete('criteria/{criterion}', [CriterionController::class, 'destroy'])->name('criterion.destroy');

    Route::get('descrobs', [DescrobController::class, 'index'])->name('descrob.index');
    Route::get('descrobs/{descrob}', [DescrobController::class, 'show'])->name('descrob.show');
    Route::post('descrobs', [DescrobController::class, 'store'])->name('descrob.store');
    Route::put('descrobs/{descrob}', [DescrobController::class, 'replace'])->name('descrob.replace');
    Route::patch('descrobs/{descrob}', [DescrobController::class, 'update'])->name('descrob.update');
    Route::delete('descrobs/{descrob}', [DescrobController::class, 'destroy'])->name('descrob.destroy');

    Route::get('rubrics', [RubricController::class, 'index'])->name('rubric.index');
    Route::get('rubrics/{rubric}', [RubricController::class, 'show'])->name('rubric.show');
    Route::post('rubrics', [RubricController::class, 'store'])->name('rubric.store');
    Route::put('rubrics/{rubric}', [RubricController::class, 'replace'])->name('rubric.replace');
    Route::patch('rubrics/{rubric}', [RubricController::class, 'update'])->name('rubric.update');
    Route::delete('rubrics/{rubric}', [RubricController::class, 'destroy'])->name('rubric.destroy');
});
