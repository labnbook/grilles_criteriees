<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AssessedStudentTest extends TestCase
{
    use RefreshDatabase;

    public string $object;
    public int $get_count;
    public string $primary_key;
    public string $get_first_key;
    public string|int $get_first_value;
    public array $get_some_value;
    public int $get_one_key;
    public array $get_one_value;
    public array $post_value;
    public array $default_value;

    public function setUp(): void
    {
        parent::setUp();

        $this->object = 'assessed-students';

        // How many results from get request
        $this->get_count = 20;

        $this->primary_key = "id_assessed_student";

        // GET ALL test
        $this->get_first_key = "data.0.id_student";
        $this->get_first_value = 0;

        // GET SOME test
        $this->get_some_value =  [
            19 => [
                "id_assessed_student" => 12,
                "id_student" => 11,
                "id_assessment" => 8,
                "missing" => 1
            ]
        ];

        // POST test
        $this->post_value = [
            "id_student" => 127,
            "id_assessment" => 6,
            "missing" => 1
        ];

        // GET ONE, PATCH and PUT tests
        $this->get_one_key = 16;
        $this->get_one_value = [
            "id_assessed_student" => $this->get_one_key,
            "id_student" => 15,
            "id_assessment" => 6,
            "missing" => 0
        ];
        $this->default_value = [
            "id_assessed_student" => $this->get_one_key,
            "id_student" => null,
            "id_assessment" => null,
            "missing" => 0
        ];
    }

    public function test_get_assessed_students_all(): void
    {
        TestHelper::assert_get_all($this);
    }

    public function test_get_assessed_students_all_filter(): void
    {
        TestHelper::assert_get_all(
            $this,
            false,
            ["object_key" => "id_assessment", "object_value" => 5, "expected_count" => 4]
        );
    }

    public function test_get_assessed_students_one(): void
    {
        TestHelper::assert_get_one($this);
    }

    public function test_get_assessed_students_none(): void
    {
        TestHelper::assert_get_none($this);
    }

    public function test_post_assessed_students_all(): void
    {
        TestHelper::assert_post_all($this);
    }

    public function test_post_assessed_students_min(): void
    {
        TestHelper::assert_post($this, ["id_student" => 99, "id_assessment" => 5]);
    }

    public function test_post_assessed_students_invalid(): void
    {
        TestHelper::assert_post_invalid($this, []);
    }

    public function test_patch_assessed_students_id_student(): void
    {
        TestHelper::assert_patch($this, ["id_student" => 152]);
    }

    public function test_patch_assessed_students_id_assessment(): void
    {
        TestHelper::assert_patch($this, ["id_assessment" => 4]);
    }

    public function test_patch_assessed_students_missing(): void
    {
        TestHelper::assert_patch($this, ["missing" => 1]);
    }

    public function test_patch_assessed_student_all(): void
    {
        TestHelper::assert_patch($this, $this->post_value);
    }

    public function test_patch_assessed_students_nothing(): void
    {
        TestHelper::assert_patch($this, []);
    }

    public function test_patch_assessed_students_invalid(): void
    {
        TestHelper::assert_patch_invalid($this, ["id_assessment", 10000000]);
    }

    public function test_put_assessed_students_min(): void
    {
        TestHelper::assert_put($this, ["id_student" => 99, "id_assessment" => 5]);
    }

    public function test_put_assessed_students_all(): void
    {
        TestHelper::assert_put($this, $this->post_value);
    }

    public function test_put_assessed_students_invalid(): void
    {
        TestHelper::assert_put_invalid($this, []);
    }

    public function test_delete_assessed_students(): void
    {
        TestHelper::assert_delete($this);
    }
}
