<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AssessmentTest extends TestCase
{
    use RefreshDatabase;

    public string $object;
    public int $get_count;
    public string $primary_key;
    public string $get_first_key;
    public string|int $get_first_value;
    public array $get_some_value;
    public int $get_one_key;
    public array $get_one_value;
    public array $post_value;
    public array $default_value;

    public function setUp(): void
    {
        parent::setUp();

        $this->object = 'assessments';

        // How many results from get request
        $this->get_count = 8;

        $this->primary_key = "id_assessment";

        // GET ALL test
        $this->get_first_key = "data.0.bonus_comment";
        $this->get_first_value = "Illum est quaerat ut temporibus.";

        // GET SOME test
        $this->get_some_value =  [
            7 => [
                "id_assessment" => 8,
                "id_production" => 5,
                "id_rubric" => 1,
                "bonus" => 5,
                "bonus_comment" => "Repellat vero cum similique enim nihil quaerat.",
                "score" => 28,
                "score_comment" => "Et dolore dolore molestiae.",
                "published" => 0,
                "criteria" => [0 => ["description" => "Soluta omnis unde nulla reiciendis cumque rerum optio incidunt."]],
                "criteria" => [1 => ["descrobs" => [4 => ["description" => "Officia et cum ducimus quia enim perspiciatis necessitatibus."]]]],
                "criteria" => [2 => ["criteria_group" => ["relative_weight" => 6.61]]],
                "descrobs" => [8 => ["description" => "Aut quis natus nihil et est nulla."]],
                "assessed_students" => [1 => ["id_assessed_student" => 12]]
            ]
        ];

        // POST test
        $this->post_value = [
            "id_production" => 3,
            "id_rubric" => 2,
            "bonus" => 3,
            "bonus_comment" => "Un commentaire de bonus !",
            "score" => 55,
            "score_comment" => "Un autre commentaire, pour le score, cette fois !",
            "published" => 0
        ];

        // GET ONE, PATCH and PUT tests
        $this->get_one_key = 5;
        $this->get_one_value = [
            "id_assessment" => $this->get_one_key,
            "id_production" => 5,
            "id_rubric" => 2,
            "bonus" => 0,
            "bonus_comment" => "Quo tempore eveniet aut doloremque sed.",
            "score" => 39,
            "score_comment" => "Sit illo id iste quod sit.",
            "published" => 0,
            "criteria" => [0 => ["description" => "Delectus enim voluptate est laboriosam dolores provident ipsa."]],
            "criteria" => [0 => ["criteria_group" => ["description" => "Ullam odit inventore doloremque minus laborum voluptatum."]]],
            "criteria" => [0 => ["descrobs" => [0 => ["description" => "Consequatur pariatur sed vero quia ut."]]]],
            "descrobs" => [1 => ["description" => "Sit quis qui nulla mollitia sed ipsum."]],
            "assessed_students" => [3 => ["id_assessed_student" => 20]]
        ];
        $this->default_value = [
            "id_assessment" => $this->get_one_key,
            "id_production" => null,
            "id_rubric" => null,
            "bonus" => null,
            "bonus_comment" => null,
            "score" => null,
            "score_comment" => null,
            "published" => 0,
        ];
    }

    // GET

    public function test_get_assessments_all(): void
    {
        TestHelper::assert_get_all($this);
    }

    public function test_get_assessments_all_norelated(): void
    {
        TestHelper::assert_get_all($this, true);
    }

    public function test_get_assessments_all_filter(): void
    {
        TestHelper::assert_get_all(
            $this,
            false,
            ["object_key" => "id_production", "object_value" => 5, "expected_count" => 3]
        );
    }

    public function test_get_assessments_one(): void
    {
        TestHelper::assert_get_one($this);
    }

    public function test_get_assessments_one_norelated(): void
    {
        TestHelper::assert_get_one($this, true);;
    }

    public function test_get_assessments_none(): void
    {
        TestHelper::assert_get_none($this);
    }

    // POST

    public function test_post_assessments_all(): void
    {
        TestHelper::assert_post_all($this);
    }

    public function test_post_assessments_min(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4]);
    }

    public function test_post_assessments_id_rubric(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "id_rubric" => 3]);
    }

    public function test_post_assessments_bonus(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "bonus" => -1]);
    }

    public function test_post_assessments_bonus_comment(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "bonus_comment" => "Whaoo, LabNbook, c'est chouette!!"]);
    }

    public function test_post_assessments_score(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "score" => 37]);
    }

    public function test_post_assessments_score_comment(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "score_comment" => "Increible!"]);
    }

    public function test_post_assessments_published(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "published" => 1]);
    }

    public function test_post_assessments_score_published(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "published" => 1, "score" => 33]);
    }

    public function test_post_assessments_score_comment_bonus_comment_id_rubric(): void
    {
        TestHelper::assert_post($this, ["id_production" => 4, "id_rubric" => 1, "score_comment" => "a est la première lettre", "bonus_comment" => "b est la deuxième!"]);
    }

    public function test_post_assessments_invalid(): void
    {
        TestHelper::assert_post_invalid($this, []);
    }

    // PATCH

    public function test_patch_assessments_id_production(): void
    {
        TestHelper::assert_patch($this, ["id_production" => 3]);
    }

    public function test_patch_assessments_id_rubric(): void
    {
        TestHelper::assert_patch($this, ["id_rubric" => 1]);
    }

    public function test_patch_assessments_bonus(): void
    {
        TestHelper::assert_patch($this, ["bonus" => -10]);
    }

    public function test_patch_assessments_bonus_comment(): void
    {
        TestHelper::assert_patch($this, ["bonus_comment" => "pas terrible..."]);
    }

    public function test_patch_assessments_score(): void
    {
        TestHelper::assert_patch($this, ["score" => 1789]);
    }

    public function test_patch_assessments_score_comment(): void
    {
        TestHelper::assert_patch($this, ["score_comment" => "Surprenant...ne vous êtes vous pas trompé de matière?"]);
    }

    public function test_patch_assessments_published(): void
    {
        TestHelper::assert_patch($this, ["published" => 1]);
    }

    public function test_patch_assessments_published_id_rubric(): void
    {
        TestHelper::assert_patch($this, ["published" => 1, "id_rubric" => 2]);
    }

    public function test_patch_assessments_score_bonus_score_comment(): void
    {
        TestHelper::assert_patch($this, ["score" => 111, "bonus" => 46, "score_comment" => "A comment for this score"]);
    }

    public function test_patch_assessments_all(): void
    {
        TestHelper::assert_patch($this, $this->post_value);
    }

    public function test_patch_assessments_nothing(): void
    {
        TestHelper::assert_patch($this, []);
    }

    public function test_patch_assessments_invalid(): void
    {
        TestHelper::assert_patch_invalid($this, ["id_production", 10000000]);
    }

    // PUT

    public function test_put_assessments_min(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1]);
    }

    public function test_put_assessments_id_rubric(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "id_rubric" => 1]);
    }

    public function test_put_assessments_bonus(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "bonus" => 1]);
    }

    public function test_put_assessments_bonus_comment(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "bonus_comment" => "One comment"]);
    }

    public function test_put_assessments_score(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "score" => 687]);
    }

    public function test_put_assessments_score_comment(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "score_comment" => "Not understandable"]);
    }

    public function test_put_assessments_published(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "published" => 1]);
    }

    public function test_put_assessments_id_rubric_published(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "published" => 1, "id_rubric" => 2]);
    }

    public function test_put_assessments_score_score_comment_bonus(): void
    {
        TestHelper::assert_put($this, ["id_production" => 1, "score" => 981, "score_comment" => "Dernier commentaire!", "bonus" => 8]);
    }

    public function test_put_assessments_all(): void
    {
        TestHelper::assert_put($this, $this->post_value);
    }

    public function test_put_assessments_invalid(): void
    {
        TestHelper::assert_put_invalid($this, []);
    }

    public function test_delete_assessments(): void
    {
        TestHelper::assert_delete($this);
    }
}
