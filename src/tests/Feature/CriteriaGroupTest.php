<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CriteriaGroupTest extends TestCase
{
    use RefreshDatabase;

    public string $object;
    public int $get_count;
    public string $primary_key;
    public string $get_first_key;
    public string|int $get_first_value;
    public array $get_some_value;
    public int $get_one_key;
    public array $get_one_value;
    public array $post_value;
    public array $default_value;

    public function setUp(): void
    {
        parent::setUp();

        $this->object = 'criteria-groups';

        // How many results from get request
        $this->get_count = 2;

        $this->primary_key = "id_criteria_group";

        // GET ALL test
        $this->get_first_key = "data.0.description";
        $this->get_first_value = "Porro voluptatem id vero perspiciatis nulla nihil.";

        // GET SOME test
        $this->get_some_value =  [
            1 => [
                "id_criteria_group" => 2,
                "id_origin" => 1,
                "position" => 1,
                "description" => "Ullam odit inventore doloremque minus laborum voluptatum.",
                "relative_weight" => 8.13
            ]
        ];

        // POST test
        $this->post_value = [
            "id_origin" => 1,
            "position" => 0,
            "description" => "Une petite description",
            "relative_weight" => 5
        ];

        // GET ONE, PATCH and PUT tests
        $this->get_one_key = 1;
        $this->get_one_value = [
            "id_criteria_group" => $this->get_one_key,
            "id_origin" => null,
            "position" => 0,
            "description" => "Porro voluptatem id vero perspiciatis nulla nihil.",
            "relative_weight" => 6.61
        ];
        $this->default_value = [
            "id_criteria_group" => $this->get_one_key,
            "id_origin" => null,
            "position" => 0,
            "description" => null,
            "relative_weight" => null
        ];
    }

    public function test_get_criteria_groups_all(): void
    {
        TestHelper::assert_get_all($this);
    }

    public function test_get_criteria_groups_one(): void
    {
        TestHelper::assert_get_one($this);
    }

    public function test_get_criteria_groups_none(): void
    {
        TestHelper::assert_get_none($this);
    }

    public function test_post_criteria_groups_all(): void
    {
        TestHelper::assert_post_all($this);
    }

    public function test_post_criteria_groups_min(): void
    {
        TestHelper::assert_post($this, []);
    }

    public function test_post_criteria_groups_id_origin(): void
    {
        TestHelper::assert_post($this, ["id_origin" => 1]);
    }

    public function test_post_criteria_groups_position(): void
    {
        TestHelper::assert_post($this, ["position" => 4]);
    }

    public function test_post_criteria_groups_description(): void
    {
        TestHelper::assert_post($this, ["description" => "hey!"]);
    }

    public function test_post_criteria_groups_relative_weight(): void
    {
        TestHelper::assert_post($this, ["relative_weight" => 3.6]);
    }

    public function test_post_criteria_groups_description_relative_weight(): void
    {
        TestHelper::assert_post($this, ["description" => "Voici une description", "relative_weight" => 3.6]);
    }

    public function test_post_criteria_groups_invalid(): void
    {
        TestHelper::assert_post_invalid($this, ["id_origin" => 10000]);
    }

    public function test_patch_criteria_groups_id_origin(): void
    {
        TestHelper::assert_patch($this, ["id_origin" => 1]);
    }

    public function test_patch_criteria_groups_position(): void
    {
        TestHelper::assert_patch($this, ["position" => 48]);
    }

    public function test_patch_criteria_groups_description(): void
    {
        TestHelper::assert_patch($this, ["description" => "This is a description"]);
    }

    public function test_patch_criteria_groups_relative_weight(): void
    {
        TestHelper::assert_patch($this, ["relative_weight" => 3.14]);
    }

    public function test_patch_criteria_groups_id_origin_position(): void
    {
        TestHelper::assert_patch($this, ["id_origin" => 1, "position" => 8]);
    }

    public function test_patch_criteria_group_all(): void
    {
        TestHelper::assert_patch($this, $this->post_value);
    }

    public function test_patch_criteria_groups_nothing(): void
    {
        TestHelper::assert_patch($this, []);
    }

    public function test_patch_criteria_groups_invalid(): void
    {
        TestHelper::assert_patch_invalid($this, ["id_origin", 100000]);
    }

    public function test_put_criteria_groups_min(): void
    {
        TestHelper::assert_put($this, []);
    }

    public function test_put_criteria_groups_id_origin(): void
    {
        TestHelper::assert_put($this, ["id_origin" => 1]);
    }

    public function test_put_criteria_groups_position(): void
    {
        TestHelper::assert_put($this, ["position" => 34]);
    }

    public function test_put_criteria_groups_description(): void
    {
        TestHelper::assert_put($this, ["description" => "claro pues"]);
    }

    public function test_put_criteria_groups_relative_weight(): void
    {
        TestHelper::assert_put($this, ["relative_weight" => 2.72]);
    }

    public function test_put_criteria_groups_description_position(): void
    {
        TestHelper::assert_put($this, ["description" => "Labnbook, c'est coool", "position" => 45]);
    }

    public function test_put_criteria_groups_all(): void
    {
        TestHelper::assert_put($this, $this->post_value);
    }

    /* Cannot delete a parent row !
    public function test_delete_criteria_groups(): void
    {
        TestHelper::assert_delete($this);
    }*/
}
