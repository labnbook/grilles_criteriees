<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CriterionTest extends TestCase
{
    use RefreshDatabase;

    public string $object;
    public int $get_count;
    public string $primary_key;
    public string $get_first_key;
    public string|int $get_first_value;
    public array $get_some_value;
    public int $get_one_key;
    public array $get_one_value;
    public array $post_value;
    public array $default_value;

    public function setUp(): void
    {
        parent::setUp();

        $this->object = 'criteria';

        // How many results from get request
        $this->get_count = 12;

        $this->primary_key = "id_criterion";

        // GET ALL test
        $this->get_first_key = "data.0.description";
        $this->get_first_value = "Soluta omnis unde nulla reiciendis cumque rerum optio incidunt.";

        // GET SOME test
        $this->get_some_value =  [
            11 => [
                "id_criterion" => 12,
                "id_origin" => null,
                "id_rubric" => 1,
                "id_section" => 3,
                "id_criteria_group" => 1,
                "type" => "descr",
                "position" => 4,
                "description" => "Aliquam sint itaque inventore iusto molestias.",
                "relative_weight" => 7.23,
                "criteria_group" => ["description" => "Porro voluptatem id vero perspiciatis nulla nihil.", "relative_weight" => 6.61],
                "descrobs" => [4 => ["description" => "Aut quis natus nihil et est nulla.", "id_descrob" => 29]]
            ]
        ];

        // POST test
        $this->post_value = [
            "id_origin" => 9,
            "id_rubric" => 4,
            "id_section" => 2,
            "id_criteria_group" => 2,
            "type" => "ob",
            "position" => 42,
            "description" => "Critère description",
            "relative_weight" => 3.14,
        ];

        // GET ONE, PATCH and PUT tests
        $this->get_one_key = 9;
        $this->get_one_value = [
            "id_criterion" => $this->get_one_key,
            "id_origin" => null,
            "id_rubric" => 1,
            "id_section" => 5,
            "id_criteria_group" => 1,
            "type" => "descr",
            "position" => 3,
            "description" => "Reiciendis ipsum non laudantium modi.",
            "relative_weight" => 2.18,
            "criteria_group" => ["description" => "Porro voluptatem id vero perspiciatis nulla nihil.", "relative_weight" => 6.61],
            "descrobs" => [4 => ["description" => "Repudiandae assumenda ducimus impedit cumque.", "id_descrob" => 48]]
        ];
        $this->default_value = [
            "id_criterion" => $this->get_one_key,
            "id_origin" => null,
            "id_rubric" => null,
            "id_section" => null,
            "id_criteria_group" => null,
            "type" => "descr",
            "position" => 0,
            "description" => null,
            "relative_weight" => null
        ];
    }

    // GET

    public function test_get_criteria_all(): void
    {
        TestHelper::assert_get_all($this);
    }

    public function test_get_criteria_all_norelated(): void
    {
        TestHelper::assert_get_all($this, true);
    }

    public function test_get_criteria_one(): void
    {
        TestHelper::assert_get_one($this);
    }

    public function test_get_criteria_one_no_related(): void
    {
        TestHelper::assert_get_one($this, true);;
    }

    public function test_get_criteria_none(): void
    {
        TestHelper::assert_get_none($this);
    }

    // POST

    public function test_post_criteria_all(): void
    {
        TestHelper::assert_post_all($this);
    }

    public function test_post_criteria_min(): void
    {
        TestHelper::assert_post($this, []);
    }

    public function test_post_criteria_id_origin(): void
    {
        TestHelper::assert_post($this, ["id_origin" => 1]);
    }

    public function test_post_criteria_id_rubric(): void
    {
        TestHelper::assert_post($this, ["id_rubric" => 3]);
    }

    public function test_post_criteria_id_section(): void
    {
        TestHelper::assert_post($this, ["id_section" => 55]);
    }

    public function test_post_criteria_id_criteria_group(): void
    {
        TestHelper::assert_post($this, ["id_criteria_group" => 2]);
    }

    public function test_post_criteria_type(): void
    {
        TestHelper::assert_post($this, ["type" => "ob"]);
    }

    public function test_post_criteria_position(): void
    {
        TestHelper::assert_post($this, ["position" => 56]);
    }

    public function test_post_criteria_description_id_section(): void
    {
        TestHelper::assert_post($this, ["description" => "Voici une description", "id_section" => 33]);
    }

    public function test_post_criteria_id_rubric_id_section_id_criteria_group(): void
    {
        TestHelper::assert_post($this, ["id_rubric" => 2, "id_section" => 22, "id_criteria_group" => 1]);
    }

    public function test_post_criteria_invalid(): void
    {
        TestHelper::assert_post_invalid($this, ["id_origin" => 10000]);
    }

    // PATCH

    public function test_patch_criteria_id_origin(): void
    {
        TestHelper::assert_patch($this, ["id_origin" => 1]);
    }

    public function test_patch_criteria_id_rubric(): void
    {
        TestHelper::assert_patch($this, ["id_rubric" => 3]);
    }

    public function test_patch_criteria_id_section(): void
    {
        TestHelper::assert_patch($this, ["id_section" => 87]);
    }

    public function test_patch_criteria_id_criteria_group(): void
    {
        TestHelper::assert_patch($this, ["id_criteria_group" => 2]);
    }

    public function test_patch_criteria_type(): void
    {
        TestHelper::assert_patch($this, ["type" => "ob"]);
    }

    public function test_patch_criteria_position(): void
    {
        TestHelper::assert_patch($this, ["position" => 2000]);
    }

    public function test_patch_criteria_description(): void
    {
        TestHelper::assert_patch($this, ["description" => "Hola amigos!"]);
    }

    public function test_patch_criteria_id_origin_position(): void
    {
        TestHelper::assert_patch($this, ["id_origin" => 1, "position" => 8]);
    }

    public function test_patch_criteria_type_description(): void
    {
        TestHelper::assert_patch($this, ["type" => "ob", "description" => "stairway to heaven"]);
    }

    public function test_patch_criteria_id_rubric_id_section_position(): void
    {
        TestHelper::assert_patch($this, ["id_rubric" => 1, "id_section" => 89, "position" => 888]);
    }

    public function test_patch_criterion_all(): void
    {
        TestHelper::assert_patch($this, $this->post_value);
    }

    public function test_patch_criteria_nothing(): void
    {
        TestHelper::assert_patch($this, []);
    }

    public function test_patch_criteria_invalid(): void
    {
        TestHelper::assert_patch_invalid($this, ["id_origin", 100000]);
    }

    // PUT

    public function test_put_criteria_min(): void
    {
        TestHelper::assert_put($this, []);
    }

    public function test_put_criteria_id_origin(): void
    {
        TestHelper::assert_put($this, ["id_origin" => 1]);
    }

    public function test_put_criteria_id_rubric(): void
    {
        TestHelper::assert_put($this, ["id_rubric" => 3]);
    }

    public function test_put_criteria_id_section(): void
    {
        TestHelper::assert_put($this, ["id_section" => 3123]);
    }

    public function test_put_criteria_id_criteria_group(): void
    {
        TestHelper::assert_put($this, ["id_criteria_group" => 2]);
    }

    public function test_put_criteria_type(): void
    {
        TestHelper::assert_put($this, ["type" => "descr"]);
    }

    public function test_put_criteria_position(): void
    {
        TestHelper::assert_put($this, ["position" => 1111]);
    }

    public function test_put_criteria_description(): void
    {
        TestHelper::assert_put($this, ["description" => "claro pues"]);
    }

    public function test_put_criteria_description_position(): void
    {
        TestHelper::assert_put($this, ["description" => "Labnbook, c'est coool", "position" => 45]);
    }

    public function test_put_criteria_id_section_position_description(): void
    {
        TestHelper::assert_put($this, ["description" => "Labnbook, c'est coool", "position" => 45, "id_section" => 456]);
    }

    public function test_put_criteria_all(): void
    {
        TestHelper::assert_put($this, $this->post_value);
    }

    /* Cannot delete a parent row !
    public function test_delete_criteria(): void
    {
        TestHelper::assert_delete($this);
    }*/
}
