<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DescrobTest extends TestCase
{
    use RefreshDatabase;

    public string $object;
    public int $get_count;
    public string $primary_key;
    public string $get_first_key;
    public string|int $get_first_value;
    public array $get_some_value;
    public int $get_one_key;
    public array $get_one_value;
    public array $post_value;
    public array $default_value;

    public function setUp(): void
    {
        parent::setUp();

        $this->object = 'descrobs';

        // How many results from get request
        $this->get_count = 60;

        $this->primary_key = "id_descrob";

        // GET ALL test
        $this->get_first_key = "data.0.description";
        $this->get_first_value = "Vel autem dolorem blanditiis.";

        // GET SOME test
        $this->get_some_value =  [
            59 => [
                "id_descrob" => 60,
                "id_origin" => 30,
                "id_criterion" => 4,
                "description" => "Est quibusdam et ratione dicta.",
                "value" => 7,
                "position" => 6
            ]
        ];

        // POST test
        $this->post_value = [
            "id_origin" => 6,
            "id_criterion" => 7,
            "description" => "Une description pour un descrobs",
            "value" => 34,
            "position" => 12
        ];

        // GET ONE, PATCH and PUT tests
        $this->get_one_key = 34;
        $this->get_one_value = [
            "id_descrob" => $this->get_one_key,
            "id_origin" => 32,
            "id_criterion" => 10,
            "description" => "Eum voluptas iure aut harum est velit quam.",
            "value" => 2,
            "position" => 2
        ];
        $this->default_value = [
            "id_descrob" => $this->get_one_key,
            "id_origin" => null,
            "id_criterion" => null,
            "description" => null,
            "value" => null,
            "position" => 0
        ];
    }

    public function test_get_descrobs_all(): void
    {
        TestHelper::assert_get_all($this);
    }

    public function test_get_descrobs_one(): void
    {
        TestHelper::assert_get_one($this);
    }

    public function test_get_descrobs_none(): void
    {
        TestHelper::assert_get_none($this);
    }

    public function test_post_descrobs_all(): void
    {
        TestHelper::assert_post_all($this);
    }

    public function test_post_descrobs_min(): void
    {
        TestHelper::assert_post($this, ["id_criterion" => 9]);
    }

    public function test_post_descrobs_id_origin(): void
    {
        TestHelper::assert_post($this, ["id_criterion" => 9, "id_origin" => 1]);
    }

    public function test_post_descrobs_description(): void
    {
        TestHelper::assert_post($this, ["id_criterion" => 9, "description" => "Voici une description!"]);
    }

    public function test_post_descrobs_value(): void
    {
        TestHelper::assert_post($this, ["id_criterion" => 9, "value" => 54]);
    }

    public function test_post_descrobs_position(): void
    {
        TestHelper::assert_post($this, ["id_criterion" => 9, "position" => 126]);
    }

    public function test_post_descrobs_description_value(): void
    {
        TestHelper::assert_post($this, ["id_criterion" => 9, "description" => "Voici une description !", "value" => 38]);
    }

    public function test_post_descrobs_invalid(): void
    {
        TestHelper::assert_post_invalid($this, ["id_criterion" => 9, "id_origin" => 10000]);
    }

    public function test_patch_descrobs_id_origin(): void
    {
        TestHelper::assert_patch($this, ["id_origin" => 3]);
    }

    public function test_patch_descrobs_description(): void
    {
        TestHelper::assert_patch($this, ["description" => "This is a description"]);
    }

    public function test_patch_descrobs_value(): void
    {
        TestHelper::assert_patch($this, ["value" => 27]);
    }
    public function test_patch_descrobs_position(): void
    {
        TestHelper::assert_patch($this, ["position" => 48]);
    }

    public function test_patch_descrobs_id_origin_position(): void
    {
        TestHelper::assert_patch($this, ["id_origin" => 6, "position" => 8]);
    }

    public function test_patch_descrob_all(): void
    {
        TestHelper::assert_patch($this, $this->post_value);
    }

    public function test_patch_descrobs_nothing(): void
    {
        TestHelper::assert_patch($this, []);
    }

    public function test_patch_descrobs_invalid(): void
    {
        TestHelper::assert_patch_invalid($this, ["id_origin", 100000]);
    }

    public function test_put_descrobs_min(): void
    {
        TestHelper::assert_put($this, ["id_criterion" => 8]);
    }

    public function test_put_descrobs_id_origin(): void
    {
        TestHelper::assert_put($this, ["id_criterion" => 8, "id_origin" => 1]);
    }

    public function test_put_descrobs_position(): void
    {
        TestHelper::assert_put($this, ["id_criterion" => 8, "position" => 34]);
    }

    public function test_put_descrobs_value(): void
    {
        TestHelper::assert_put($this, ["id_criterion" => 8, "value" => 2.72]);
    }

    public function test_put_descrobs_description(): void
    {
        TestHelper::assert_put($this, ["id_criterion" => 8, "description" => "claro pues"]);
    }

    public function test_put_descrobs_description_position(): void
    {
        TestHelper::assert_put($this, ["id_criterion" => 8, "description" => "Labnbook, c'est coool", "position" => 45]);
    }

    public function test_put_descrobs_all(): void
    {
        TestHelper::assert_put($this, $this->post_value);
    }

    public function test_put_descrobs_invalid(): void
    {
        TestHelper::assert_put_invalid($this, []);
    }

    public function test_delete_descrobs(): void
    {
        TestHelper::assert_delete($this);
    }
}
