<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RubricTest extends TestCase
{
    use RefreshDatabase;

    public string $object;
    public int $get_count;
    public string $primary_key;
    public string $get_first_key;
    public string|int $get_first_value;
    public array $get_some_value;
    public int $get_one_key;
    public array $get_one_value;
    public array $post_value;
    public array $default_value;

    public function setUp(): void
    {
        parent::setUp();

        $this->object = 'rubrics';

        // How many results from get request
        $this->get_count = 4;

        $this->primary_key = "id_rubric";

        // GET ALL test
        $this->get_first_key = "data.0.name";
        $this->get_first_value = "non sint similique";

        // GET SOME test
        $this->get_some_value =  [
            3 => [
                "id_rubric" => 4,
                "id_activity" => 1,
                "name" => "ad commodi esse",
                "grading" => 15,
                "min_grade" => 9,
                "max_grade" => 81,
                "min_bonus" => -8,
                "max_bonus" => 6,
                "criteria" => [0 => ["description" => "Delectus enim qui in repellendus non est amet.", "relative_weight" => 2.97]],
                "criteria" => [0 => ["criteria_group" => ["description" => "Ullam odit inventore doloremque minus laborum voluptatum."]]],
                "criteria" => [0 => ["descrobs" => [2 => ["id_descrob" => 35, "description" => "Pariatur quod aperiam est quae."]]]]
            ]
        ];

        // POST test
        $this->post_value = [
            "id_activity" => 2,
            "name" => "Un ptit nom pour la rubric",
            "grading" => 18,
            "min_grade" => 2,
            "max_grade" => 36,
            "min_bonus" => -1,
            "max_bonus" => 4,
        ];

        // GET ONE, PATCH and PUT tests
        $this->get_one_key = 2;
        $this->get_one_value = [
            "id_rubric" => $this->get_one_key,
            "id_activity" => 5,
            "name" => "ex voluptatibus et",
            "grading" => 17,
            "min_grade" => 0,
            "max_grade" => 84,
            "min_bonus" => 0,
            "max_bonus" => 3
        ];
        $this->default_value = [
            "id_rubric" => $this->get_one_key,
            "id_activity" => null,
            "name" => null,
            "grading" => null,
            "min_grade" => null,
            "max_grade" => null,
            "min_bonus" => null,
            "max_bonus" => null
        ];
    }

    // GET

    public function test_get_rubrics_all(): void
    {
        TestHelper::assert_get_all($this);
    }

    public function test_get_rubrics_all_norelated(): void
    {
        TestHelper::assert_get_all($this, true);
    }

    public function test_get_rubrics_all_filter(): void
    {
        TestHelper::assert_get_all(
            $this,
            false,
            ["object_key" => "id_activity", "object_value" => 5, "expected_count" => 2]
        );
    }

    public function test_get_rubrics_one(): void
    {
        TestHelper::assert_get_one($this);
    }

    public function test_get_rubrics_one_norelated(): void
    {
        TestHelper::assert_get_one($this, true);;
    }

    public function test_get_rubrics_none(): void
    {
        TestHelper::assert_get_none($this);
    }

    // POST

    public function test_post_rubrics_all(): void
    {
        TestHelper::assert_post_all($this);
    }

    public function test_post_rubrics_min(): void
    {
        TestHelper::assert_post($this, []);
    }

    public function test_post_rubrics_id_activity(): void
    {
        TestHelper::assert_post($this, ["id_activity" => 1]);
    }

    public function test_post_rubrics_name(): void
    {
        TestHelper::assert_post($this, ["name" => "un p'tit nom"]);
    }

    public function test_post_rubrics_grading(): void
    {
        TestHelper::assert_post($this, ["grading" => 22]);
    }

    public function test_post_rubrics_min_grade(): void
    {
        TestHelper::assert_post($this, ["min_grade" => 1]);
    }

    public function test_post_rubrics_max_grade(): void
    {
        TestHelper::assert_post($this, ["max_grade" => 24]);
    }

    public function test_post_rubrics_min_bonus(): void
    {
        TestHelper::assert_post($this, ["min_bonus" => -2]);
    }

    public function test_post_rubrics_max_bonus(): void
    {
        TestHelper::assert_post($this, ["max_bonus" => 2]);
    }

    public function test_post_rubrics_name_max_bonus(): void
    {
        TestHelper::assert_post($this, ["max_bonus" => 2, "name" => "What's your name ?"]);
    }

    public function test_post_rubrics_name_max_bonus_min_grade_grading(): void
    {
        TestHelper::assert_post($this, ["max_bonus" => 2, "name" => "What's your name ?", "min_grade" => 3, "grading" => 78]);
    }

    // PATCH

    public function test_patch_rubrics_id_activity(): void
    {
        TestHelper::assert_patch($this, ["id_activity" => 4]);
    }

    public function test_patch_rubrics_name(): void
    {
        TestHelper::assert_patch($this, ["name" => "Cual es tu nombre?"]);
    }

    public function test_patch_rubrics_grading(): void
    {
        TestHelper::assert_patch($this, ["grading" => 567]);
    }

    public function test_patch_rubrics_min_grade(): void
    {
        TestHelper::assert_patch($this, ["min_grade" => 8]);
    }

    public function test_patch_rubrics_max_grade(): void
    {
        TestHelper::assert_patch($this, ["max_grade" => 38]);
    }

    public function test_patch_rubrics_min_bonus(): void
    {
        TestHelper::assert_patch($this, ["min_bonus" => 12]);
    }

    public function test_patch_rubrics_max_bonus(): void
    {
        TestHelper::assert_patch($this, ["max_bonus" => 13]);
    }

    public function test_patch_rubrics_min_bonus_name(): void
    {
        TestHelper::assert_patch($this, ["min_bonus" => 13, "name" => "LabNbook team"]);
    }

    public function test_patch_rubrics_min_bonus_name_grading_min_grade(): void
    {
        TestHelper::assert_patch($this, ["min_bonus" => 13, "name" => "LabNbook team", "grading" => 45, "min_grade" => 4]);
    }

    public function test_patch_rubric_all(): void
    {
        TestHelper::assert_patch($this, $this->post_value);
    }

    public function test_patch_rubrics_nothing(): void
    {
        TestHelper::assert_patch($this, []);
    }

    public function test_patch_rubrics_invalid(): void
    {
        TestHelper::assert_patch_invalid($this, ["id_activity", 10000000]);
    }

    // PUT

    public function test_put_rubrics_min(): void
    {
        TestHelper::assert_put($this, []);
    }

    public function test_put_rubrics_all(): void
    {
        TestHelper::assert_put($this, $this->post_value);
    }

    public function test_put_rubrics_id_activity(): void
    {
        TestHelper::assert_put($this, ["id_activity" => 4]);
    }

    public function test_put_rubrics_name(): void
    {
        TestHelper::assert_put($this, ["name" => "name test"]);
    }

    public function test_put_rubrics_grading(): void
    {
        TestHelper::assert_put($this, ["grading" => 100]);
    }

    public function test_put_rubrics_min_grade(): void
    {
        TestHelper::assert_put($this, ["min_grade" => 1]);
    }

    public function test_put_rubrics_max_grade(): void
    {
        TestHelper::assert_put($this, ["max_grade" => 10]);
    }

    public function test_put_rubrics_min_bonus(): void
    {
        TestHelper::assert_put($this, ["min_bonus" => 1]);
    }

    public function test_put_rubrics_max_bonus(): void
    {
        TestHelper::assert_put($this, ["max_bonus" => 100]);
    }

    public function test_put_rubrics_max_bonus_grading(): void
    {
        TestHelper::assert_put($this, ["max_bonus" => 100, "grading" => 125]);
    }

    public function test_put_rubrics_max_bonus_grading_min_grade(): void
    {
        TestHelper::assert_put($this, ["max_bonus" => 100, "grading" => 125, "min_grade" => 6]);
    }

    public function test_delete_rubrics(): void
    {
        TestHelper::assert_delete($this);
    }
}
