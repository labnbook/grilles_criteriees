<?php

namespace Tests\Feature;

class TestHelper
{
    /**
     * If the parameter "norelated" is true, modifies the query string and remove related objects checking
     * @param object $t
     * @param bool $no_related
     * @return array
     */
    private static function _no_related_configuration(object $t, bool $no_related): array
    {
        $query_string = '';
        $has_query_string = false;
        $get_some_value = $t->get_some_value;
        if ($no_related){
            $has_query_string = true;
            $query_string .= '?norelated=true';
            // remove all nested arrays
            foreach($get_some_value[array_key_first($get_some_value)] as $key => $value) {
                if (is_array($value)){
                    unset($get_some_value[array_key_first($get_some_value)][$key]);
                }
            }
        }
        return [$query_string, $has_query_string, $get_some_value];
    }

    /**
     * When a patch or a put request modifies related data key, remove related objects checking
     * @param object $t
     * @param array $data
     * @return array
     */
    private static function _delete_nested_keys_if_required(object $t, array $data): array
    {
        $one_value = $t->get_one_value;
        foreach ($one_value as $key => $value) {
            if (is_array($value) && array_key_exists("id_" . $key, $data)){
                unset($one_value[$key]);
            }
        }
        return $one_value;
    }

    public static function assert_get_all($t, $no_related=false, $filter=["object_key" => ""]): void
    {
        [$query_string, $has_query_string, $get_some_value] = self::_no_related_configuration($t, $no_related);
        $t
            ->get(config('app.url') . '/api/v1/' . $t->object . $query_string)
            ->assertStatus(200)
            ->assertJsonCount($t->get_count, "data")
            ->assertJsonPath($t->get_first_key, $t->get_first_value)
            ->assertJson(["data" => $get_some_value]);

        // handle filter argument
        if ($filter["object_key"] !== ""){
            $query_string .= $has_query_string ? '&' : '?';
            $query_string .= $filter["object_key"] . '=' . $filter["object_value"];
            // Assert that every json entry respects the filtering constraint
            $t
                ->get(config('app.url') . '/api/v1/' . $t->object . $query_string)
                ->assertStatus(200)
                ->assertJsonPath(
                    "data.*." . $filter["object_key"],
                    array_fill(0, $filter["expected_count"], $filter["object_value"])
                );
        }
    }

    public static function assert_get_one($t, $no_related=false): void
    {
        [$query_string, $has_query_string, $get_some_value] = self::_no_related_configuration($t, $no_related);
        $t
            ->get(config('app.url') . '/api/v1/' . $t->object . $query_string)
            ->assertStatus(200)
            ->assertJsonPath($t->get_first_key, $t->get_first_value)
            ->assertJson(["data" => $get_some_value]);
    }

    public static function assert_get_none($t): void
    {
        $t
            ->get(config('app.url') . '/api/v1/' . $t->object . '/10000000')
            ->assertStatus(404);
    }

    public static function assert_post_all($t): void
    {
        $t
            ->postJson(config('app.url') . '/api/v1/' . $t->object, $t->post_value)
            ->assertStatus(201)
            ->assertJson(["data" => array_merge($t->post_value, [$t->primary_key => $t->get_count + 1])]);
    }

    public static function assert_post($t, $data): void
    {
        $t
            ->postJson(config('app.url') . '/api/v1/' . $t->object, $data)
            ->assertStatus(201)
            ->assertJson(["data" => array_merge($t->default_value, $data, [$t->primary_key => $t->get_count + 1])]);;
    }

    public static function assert_post_invalid($t, $data): void
    {
        $t
            ->postJson(config('app.url') . '/api/v1/' . $t->object, $data)
            ->assertStatus(400);
    }

    public static function assert_patch($t, $data): void
    {
        $one_value = self::_delete_nested_keys_if_required($t, $data);
        $t
            ->patchJson(config('app.url') . '/api/v1/' . $t->object . '/' . $t->get_one_key, $data)
            ->assertStatus(200)
            ->assertJson(["data" => array_merge($one_value, $data)]);
    }

    public static function assert_patch_invalid($t, $data): void
    {
        $t
            ->patchJson(config('app.url') . '/api/v1/' . $t->object, $data)
            ->assertStatus(405);
    }

    public static function assert_put($t, $data): void
    {
        $t
            ->putJson(config('app.url') . '/api/v1/' . $t->object . '/' . $t->get_one_key, $data)
            ->assertStatus(200)
            ->assertJson(["data" => array_merge($t->default_value, $data)]);
    }

    public static function assert_put_invalid($t, $data): void
    {
        $t
            ->putJson(config('app.url') . '/api/v1/' . $t->object . '/' . $t->get_one_key, $data)
            ->assertStatus(400);;
    }

    public static function assert_delete($t): void
    {
        $t
            ->delete(config('app.url') . '/api/v1/' . $t->object . '/' . $t->get_one_key)
            ->assertStatus(204);
        $t
            ->get(config('app.url') . '/api/v1/' . $t->object . '/' . $t->get_one_key)
            ->assertStatus(404);
    }
}
